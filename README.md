Hướng dẫn cài đặt source lên localhost:

phần 1: tài nguyên yêu cầu:

1: clone source từ git về: git clone https://tantn1256@bitbucket.org/tantn1256/test_php_inter.git

2: cài đặt môi trường: xampp

	PHP version 5.6.3
	
	MYSQL version 5.6.21
	
phần 2: các bước cài đặt

1: coppy source từ git vào thư mục :\xampp\htdocs

2: import database. database file nằm ở thư mục gốc của source dự án "ci_codeigniter/webproduct.sql"

3: chỉnh lại file config để khớp với databases name trên localhost của bạn.

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'webproduct',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
);
4: chỉnh lại base_url tại file config.php

$config['base_url'] = 'http://localhost/ci_codeigniter/';

5: truy cập vào trang website và đâng nhập bằng tài khoản sau:

username: admin

password: 123456


<?php
	class m_nguoi_dung_table extends CI_Model
	{
		public function ds_nguoi_dung()
		{
			$query=$this->db->query('select * from nguoi_dung');
			if($query->num_rows()>0)
				return $query->result_array();
			return false;
		}
		public function nguoi_dung_id($id)
		{
			$query=$this->db->query('select * from nguoi_dung where ma_nguoi_dung=?',array($id));
			if($query->num_rows()>0)
				return $query->row_array();
			return false;
		}
		public function them_nguoi_dung($data, $data_images)
		{
			$chuoiSQL='insert into nguoi_dung(`ten_nguoi_dung`, `ngay_sinh`, `gioi_tinh`, `dia_chi`, `email`, `dien_thoai`,`image_nguoidung`)values(?,?,?,?,?,?,?)';
			$result=$this->db->query($chuoiSQL,array($data['ten_nguoi_dung'],$data['ngay_sinh'],$data['gioi_tinh'],$data['dia_chi'],$data['email'],$data['dien_thoai'],$data_images));
			return $result;
		}
		public function capnhat($id)
		{
				$this->db->select('*');
                $this->db->from('nguoi_dung');
                $this->db->where('ma_nguoi_dung',$id );
                $query = $this->db->get();
                return $result = $query->row_array();
		}
		 public function capnhat_xl($id, $data_images) 
		 {
              $data = array(
			    'ten_nguoi_dung' => $this->input->post('ten_nguoi_dung'),
			    'ngay_sinh' => $this->input->post('ngay_sinh'),
			    'gioi_tinh' => $this->input->post('gioi_tinh'),
			    'dia_chi' => $this->input->post('dia_chi'),
			    'email' => $this->input->post('email'),
			    'dien_thoai' => $this->input->post('dien_thoai'),
			    'image_nguoidung' => $data_images
                );

                $this->db->where('ma_nguoi_dung', $id);
                $this->db->update('nguoi_dung', $data);


        } 
        public function xoa($id) 
		{
             $this->db->where('ma_nguoi_dung',$id );
              $this->db->delete('nguoi_dung');
        }
	}
?>
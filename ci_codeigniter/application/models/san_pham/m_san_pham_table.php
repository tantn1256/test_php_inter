<?php
class m_san_pham_table extends CI_Model
{
	public function getSanPham()
	{
		$query= $this->db->get('san_pham');
		if($query->num_rows()>0)
			return $query->result();
		return false;
	}
	public function getSanPhamId($id)
	{
		$this->db->where(array('masanpham'=>$id));
		$query= $this->db->get('san_pham');
		if($query->num_rows()>0)
			return $query->row();
		return false;
	}
}
?>

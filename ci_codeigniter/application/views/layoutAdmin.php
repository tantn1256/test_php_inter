<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-withd, initial-scale=1">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title><?php echo $title_bar;?></title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!--<link href="<?php //echo base_url()?>public/css_js/css/bootstrap.min.css" rel="stylesheet">!-->
	<link href="<?php echo base_url()?>public/css/sticky-footer-navbar.css" rel="stylesheet">
	<script src="<?php echo base_url()?>public/js/ie-emulation-modes-warning.js"></script>
</head>
<body>
	<!--menu-->
	<?php $this->load->view('menuAdmin') ?>
	<!--content-->
	
	<div class="container-fluid">
		<?php
			if(isset($path))
			{
				foreach ($path as $path_view) {
					$this->load->view($path_view);
				}
			}
		?>
	</div>
	<!--End content-->
	<?php $this->load->view('footer') ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php echo base_url()?>public/css_js/js/bootstrap.min.js"></script>
	<!--Just to make our placeholder images work. don't actually copy the next line!-->
	<script src="<?php echo base_url()?>public/css_js/js/vendor/holder.js"></script>
	<!--IE10 viewport hack for surface/desktop windows 8 bug!-->
	<script src="<?php echo base_url()?>public/css_js/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>


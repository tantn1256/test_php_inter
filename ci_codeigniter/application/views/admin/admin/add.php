<?php
	//load ra file head
	$this->load->view('admin/admin/head', $this->data);
?>
<div class="line"></div>
<div class="wrapper">
	<div class="widget">
		<div class="title">
			<h6>Thêm mới quản trị viên</h6>
		</div>


		<form class="form" id="form" action="" method="post" enctype="multipart/form-data">
    	<fieldset>
    	<div class="formRow">
			<label class="formLeft" for="param_name">Name:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="text" name="name" id="param_name" _autocheck="true"  value="<?php echo set_value('name')?>">
				</span>
				<span name="name_autocheck" class="autocheck"></span>
				<div name="name_error" class="clear error"><?php echo form_error('name') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		  <div class="formRow">
			<label class="formLeft" for="param_name">Username:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="text" name="username" id="param_username" _autocheck="true" value="<?php echo set_value('username')?>">
				</span>
				<span name="username_autocheck" class="autocheck"></span>
				<div name="username_error" class="clear error"><?php echo form_error('username') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formRow">
			<label class="formLeft" for="param_name">Password:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="password" name="password" id="param_password" _autocheck="true" >
				</span>
				<span name="name_autocheck" class="autocheck"></span>
				<div name="name_error" class="clear error"><?php echo form_error('password') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formRow">
			<label class="formLeft" for="param_name">Nhập lại Password:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="password" name="re_password" id="param_re_password" _autocheck="true" >
				</span>
				<span name="name_autocheck" class="autocheck"></span>
				<div name="name_error" class="clear error"><?php echo form_error('re_password') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formSubmit">
   			<input type="submit" name='submit'value="Thêm mới" class="redB">
   			<input type="reset"  name='reset' value="Hủy bỏ" class="basic">
	    </div>
    	</fieldset>
   	</form>
    </div>
</div>
<?php
	//load ra file head
	$this->load->view('admin/user/head', $this->data);
?>
<div class="line"></div>
<div class="wrapper">
	<div class="widget">
		<div class="title">
			<h6>Thêm mới quản trị viên</h6>
		</div>


		<form class="form" id="form" action="" method="post" enctype="multipart/form-data">
    	<fieldset>
    	<div class="formRow">
			<label class="formLeft" for="param_name">Name:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="text" name="name" id="param_name" _autocheck="true"  value="<?php echo set_value('name')?>">
				</span>
				<span name="name_autocheck" class="autocheck"></span>
				<div name="name_error" class="clear error"><?php echo form_error('name') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		 <div class="formRow">
			<label class="formLeft" for="param_name">Email:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="text" name="email" id="param_email" _autocheck="true" value="<?php echo set_value('email')?>">
				</span>
				<span name="username_autocheck" class="autocheck"></span>
				<div name="username_error" class="clear error"><?php echo form_error('email') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		 <div class="formRow">
			<label class="formLeft" for="param_name">Phone:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="text" name="phone" id="param_phone" _autocheck="true" value="<?php echo set_value('phone')?>">
				</span>
				<span name="username_autocheck" class="autocheck"></span>
				<div name="username_error" class="clear error"><?php echo form_error('phone') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		 <div class="formRow">
			<label class="formLeft" for="param_name">Address:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="text" name="address" id="param_address" _autocheck="true" value="<?php echo set_value('address')?>">
				</span>
				<span name="username_autocheck" class="autocheck"></span>
				<div name="username_error" class="clear error"><?php echo form_error('address') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formRow">
			<label class="formLeft" for="param_name">Password:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="password" name="password" id="param_password" _autocheck="true" >
				</span>
				<span name="name_autocheck" class="autocheck"></span>
				<div name="name_error" class="clear error"><?php echo form_error('password') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formRow">
			<label class="formLeft" for="param_name">Nhập lại Password:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="password" name="re_password" id="param_re_password" _autocheck="true" >
				</span>
				<span name="name_autocheck" class="autocheck"></span>
				<div name="name_error" class="clear error"><?php echo form_error('re_password') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formRow">
			<label class="formLeft" for="param_name">Created:<span class="req">*</span></label>
			<div class="formRight">
				<span class="oneTwo">
					<input type="date" name="created" id="param_created" _autocheck="true" value="<?php echo set_value('created')?>">
				</span>
				<span name="name_autocheck" class="autocheck"></span>
				<div name="name_error" class="clear error"><?php echo form_error('created') ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formSubmit">
   			<input type="submit" name='submit'value="Thêm mới" class="redB">
   			<input type="reset"  name='reset' value="Hủy bỏ" class="basic">
	    </div>
    	</fieldset>
   	</form>
    </div>
</div>
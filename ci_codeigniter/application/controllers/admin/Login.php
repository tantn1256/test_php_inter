<?php
Class Login extends MY_controller
{
	function index()
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
		//neu ma co du lieu post len thi kiem tra
		if($this->input->post('submit'))
		{
			$this->form_validation->set_rules('login', 'login', 'callback_check_login');
			//nhập liệu chính xác
			if($this->form_validation->run())
			{
				$this->session->set_userdata('login', true);
				//chuyen toi trang danh sach quan tri vien
				redirect(admin_url('home'));
			}
		}
		$this->load->view('admin/login/index');
	}
	//kiểm tra username và password chinh xac không
	function check_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$password = md5($password);

		$this->load->model('admin_model');
		$where = array('username' => $username, 'password' => $password);
		//kiểm tra username đã tồn tại chưa
		if($this->admin_model->check_exists($where))
		{
			return true;
		}
		//trả về thông báo lổi
		$this->form_validation->set_message(__FUNCTION__, 'Không đăng nhập thành công');
		return false;
	}
}

?>
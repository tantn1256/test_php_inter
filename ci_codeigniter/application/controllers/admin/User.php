<?php
class User extends My_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}
	//lay danh sach user
	function index()
	{
		$input = array();
		$list = $this->user_model->get_list($input);
		$this->data['list'] = $list;

		$total = $this->user_model->get_total($input);
		$this->data['total'] = $total;

		//lay noi dung cua bien message
		$message = $this->session->flashdata('message');
		$this->data['message']= $message;

		$this->data['temp'] = 'admin/user/index';
		$this->load->view('admin/main', $this->data);
	}
	//kiểm tra email đã tồn tại chưa
	function check_email()
	{
		$email = $this->input->post('email');
		$where = array('email' => $email);
		//kiểm tra email đã tồn tại chưa
		if($this->user_model->check_exists($where))
		{
			//trả về thông báo lổi
			$this->form_validation->set_message(__FUNCTION__, 'Email đã tồn tại');
			return false;
		}
		return true;
	}
	function add()
	{
		
		$this->load->library('form_validation');
		$this->load->helper('form');
		//neu ma co du lieu post len thi kiem tra
		if($this->input->post('submit'))
		{
			$this->form_validation->set_rules('name', 'Tên', 'required|min_length[8]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_email');
			$this->form_validation->set_rules('phone', 'Điện Thoại', 'required|numeric');
			$this->form_validation->set_rules('address', 'Địa chỉ', 'required');
			$this->form_validation->set_rules('created', 'Ngày', 'required');
			$this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]');
			$this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu','required|matches[password]');
			//nhập liệu chính xác
			if($this->form_validation->run())
			{
				//thêm vào cơ sở dữ liệu
				$name     = $this->input->post('name');
				$email    = $this->input->post('email');
				$phone    = $this->input->post('phone');
				$address  = $this->input->post('address');
				$created  = $this->input->post('created');
				$password = $this->input->post('password');
				$data = array(
					'name'     => $name,
					'email'    => $email,
					'phone'    => $phone,
					'address'  => $address,
					'created'  => $created,
					'password' => md5($password)

				);
				if($this->user_model->create($data))
				{
					//tao noi dung thong bao
					//ham session luu lai noi dung nao do
					$this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
				}else
				{
					$this->session->set_flashdata('message', 'Không thêm thành công');
				}
				//chuyen toi trang danh sach quan ly thanh vien
				redirect(admin_url('user'));
			}
		}
		
		$this->data['temp'] = 'admin/user/add';
		$this->load->view('admin/main', $this->data);
	}
	function edit()
	{
		//lay id quan trị vien cần chỉnh sữa
		$id = $this->uri->rsegment('3');
		$id = intval($id);

		$this->load->library('form_validation');
		$this->load->helper('form');

		//lấy thông tin của thành viên
		$info = $this->user_model->get_info($id);
		if(!$info)
		{
			$this->session->set_flashdata('message', 'Không tồn tại thành viên');
			redirect(admin_url('user'));
		}
		$this->data['info'] = $info;
		if($this->input->post('submit'))
		{
			$this->form_validation->set_rules('name', 'Tên', 'required|min_length[8]');
			if($info->email != $this->input->post('email'))
			{
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_email');
			}			
			$this->form_validation->set_rules('phone', 'Điện Thoại', 'required|numeric');
			$this->form_validation->set_rules('address', 'Địa chỉ', 'required');
			$this->form_validation->set_rules('created', 'Ngày', 'required');
			$password = $this->input->post('password');
			if($password)
			{
				$this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]');
				$this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu','required|matches[password]');
			}
			if($this->form_validation->run())
			{
				//thêm vào cơ sở dữ liệu
				$name     = $this->input->post('name');
				$email    = $this->input->post('email');
				$phone    = $this->input->post('phone');
				$address  = $this->input->post('address');
				$created  = $this->input->post('created');
				$data = array(
					'name'     => $name,
					'email'    => $email,
					'phone'    => $phone,
					'address'  => $address,
					'created'  => $created
				);
				//neu ma thay doi mat khau thi moi gan du lieu
				if($password)
				{
					$data['password'] = md5($password);
				}
				if($this->user_model->update($id, $data))
				{
					//tao noi dung thong bao
					//ham session luu lai noi dung nao do
					$this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
				}else
				{
					$this->session->set_flashdata('message', 'Không cập nhật được');
				}
				//chuyen toi trang danh sach quan ly thanh vien
				redirect(admin_url('user'));
			}
			
		}
		$this->data['temp'] = 'admin/user/edit';
		$this->load->view('admin/main', $this->data);
	}
	function delete()
	{
		//lay id quan trị vien cần xoa
		$id = $this->uri->rsegment('3');
		$id = intval($id);
		//lấy thông tin của quản trị viên
		$info = $this->user_model->get_info($id);
		if(!$info)
		{
			$this->session->set_flashdata('message', 'Không tồn tại Thành viên');
			redirect(admin_url('user'));
		}
		//thuc hien xoa
		$this->user_model->delete($id);
		$this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
			redirect(admin_url('user'));
	}	
	function detail()
	{
		//lay id quan trị vien cần xem
		$id = $this->uri->rsegment('3');
		$id = intval($id);
		//lấy thông tin của user
		$info = $this->user_model->get_info($id);
		if(!$info)
		{
			$this->session->set_flashdata('message', 'Không tồn tại Thành viên');
			redirect(admin_url('user'));
		}
		$this->data['info'] = $info;
		//thuc hien xem
		$this->data['temp'] = 'admin/user/detail';
		$this->load->view('admin/main', $this->data);
	}	
}
?>
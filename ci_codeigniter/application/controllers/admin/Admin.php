<?php
class Admin extends My_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	//lay danh sach admin
	function index()
	{
		$input = array();
		$list = $this->admin_model->get_list($input);
		$this->data['list'] = $list;

		$total = $this->admin_model->get_total($input);
		$this->data['total'] = $total;

		//lay noi dung cua bien message
		$message = $this->session->flashdata('message');
		$this->data['message']= $message;

		//load view
		$this->data['temp'] = 'admin/admin/index';
		$this->load->view('admin/main', $this->data);
	}
	//kiểm tra username đã tồn tại chưa
	function check_username()
	{
		$username = $this->input->post('username');
		$where = array('username' => $username);
		//kiểm tra username đã tồn tại chưa
		if($this->admin_model->check_exists($where))
		{
			//trả về thông báo lổi
			$this->form_validation->set_message(__FUNCTION__, 'Tài khoản đã tồn tại');
			return false;
		}
		return true;
	}
	// them moi quan tri vien
	function add()
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
		//neu ma co du lieu post len thi kiem tra
		if($this->input->post('submit'))
		{
			$this->form_validation->set_rules('name', 'Tên', 'required|min_length[8]');
			$this->form_validation->set_rules('username', 'Tài khoản đăng nhập', 'required|callback_check_username');
			$this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]');
			$this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu','required|matches[password]');
			//nhập liệu chính xác
			if($this->form_validation->run())
			{
				//thêm vào cơ sở dữ liệu
				$name     = $this->input->post('name');
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$data = array(
					'name'     => $name,
					'username' => $username,
					'password' => md5($password)

				);
				if($this->admin_model->create($data))
				{
					//tao noi dung thong bao
					//ham session luu lai noi dung nao do
					$this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
				}else
				{
					$this->session->set_flashdata('message', 'Không thêm thành công');
				}
				//chuyen toi trang danh sach quan tri vien
				redirect(admin_url('admin'));
			}
		}
		
		$this->data['temp'] = 'admin/admin/add';
		$this->load->view('admin/main', $this->data);
	}
	//hàm chỉnh sữa thông tin quản trị viên
	function edit()
	{
		//lay id quan trị vien cần chỉnh sữa
		$id = $this->uri->rsegment('3');
		$id = intval($id);

		$this->load->library('form_validation');
		$this->load->helper('form');

		//lấy thông tin của quản trị viên
		$info = $this->admin_model->get_info($id);
		if(!$info)
		{
			$this->session->set_flashdata('message', 'Không tồn tại quản trị viên');
			redirect(admin_url('admin'));
		}
		$this->data['info'] = $info;
		if($this->input->post('submit'))
		{
			$this->form_validation->set_rules('name', 'Tên', 'required|min_length[8]');
			if($info->username != $this->input->post('username'))
			{
			$this->form_validation->set_rules('username', 'Tài khoản đăng nhập', 'required|callback_check_username');
			}
			$password = $this->input->post('password');
			if($password)
			{
				$this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]');
				$this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu','required|matches[password]');
			}
			if($this->form_validation->run())
			{
				//thêm vào cơ sở dữ liệu
				$name     = $this->input->post('name');
				$username = $this->input->post('username');
				$data = array(
					'name'     => $name,
					'username' => $username,
				);
				//neu ma thay doi mat khau thi moi gan du lieu
				if($password)
				{
					$data['password'] = md5($password);
				}
				if($this->admin_model->update($id, $data))
				{
					//tao noi dung thong bao
					//ham session luu lai noi dung nao do
					$this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
				}else
				{
					$this->session->set_flashdata('message', 'Không cập nhật được');
				}
				//chuyen toi trang danh sach quan tri vien
				redirect(admin_url('admin'));
			}
			
		}
		$this->data['temp'] = 'admin/admin/edit';
		$this->load->view('admin/main', $this->data);
	}
	function delete()
	{
		//lay id quan trị vien cần xoa
		$id = $this->uri->rsegment('3');
		$id = intval($id);
		//lấy thông tin của quản trị viên
		$info = $this->admin_model->get_info($id);
		if(!$info)
		{
			$this->session->set_flashdata('message', 'Không tồn tại quản trị viên');
			redirect(admin_url('admin'));
		}
		//thuc hien xoa
		$this->admin_model->delete($id);
		$this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
			redirect(admin_url('admin'));
	}
	function logout()
	{
		if($this->session->userdata('login'))
		{
			$this->session->unset_userdata('login');
		}
		redirect(admin_url('login'));
	}
}
?>